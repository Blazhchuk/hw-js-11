"use strict"




// ex 1

const btn = document.getElementById('btn-click');
const content = document.getElementById('content');

function addParagraph() {

    for (let i = 0; i < 1; i++) {
        let createP = document.createElement('p');
        createP.value = i;
        createP.textContent = 'New Paragraph';
        content.appendChild(createP);
    }
}

btn.addEventListener('click', addParagraph);


// ex 2

let footer = document.querySelector('footer');

let newBtn = document.createElement('button');
newBtn.id = 'btn-input-create';
newBtn.textContent = 'Crate Input';

content.appendChild(newBtn);

let inputContainer = document.createElement('div');
content.appendChild(inputContainer);

newBtn.addEventListener('click', () => {
    let input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Enter something:';
    input.name = 'new input';
    inputContainer.appendChild(input);
})
